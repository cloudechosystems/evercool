<?php get_header(); ?>
<section class="inner_page_banner" style="background-image: url('<?php echo get_field('banner_image') ?>')" >
	<div class="container">
		<h1 class="page_title">
	    <?php echo get_field('banner_heading') ?>
		</h1>
		<div class="page_description">
	    <?php echo get_field('banner_sub_heading') ?>
		</div>
	</div>
</section>
<section>
          <div class="container">
           <div class="row">
						 <?php
						 while ( have_posts() ) :
								 the_post();?>
            <div class="col-md-9">

                          <div class="single_post">
                          <div class="single_post_content">
                              <?php the_content(); ?>
                          </div>
                           </div>

                           <ul class="nav nav-tabs " id="myTab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="course-summary-tab" data-toggle="tab" href="#course-summary" role="tab" aria-controls="course-summary" aria-selected="true">Course Summary</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="schedule-pricing-tab" data-toggle="tab" href="#schedule-pricing" role="tab" aria-controls="schedule-pricing" aria-selected="false">Schedule & pricing</a>
                            </li>
                          </ul>
                            <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="course-summary" role="tabpanel" aria-labelledby="course-summary-tab">
                           <?php
                           if( have_rows('course_summary') ):
                              $i=1;
                             while ( have_rows('course_summary') ) : the_row();?>
                             <div class="summary-title">
                               <h3>
                               <?= get_sub_field('summary_title'); ?>
                             </h3>
                             </div>
                             <div class="summary-content">
                               <?= get_sub_field('summary_content'); ?>
                             </div>

                             <?php
                             if( have_rows('syllabus') ):
                              ?>
                               <div id="accordion">
                               <?php while ( have_rows('syllabus') ) : the_row();?>
                                    <div class="card">
                                      <div class="card-header" id="heading<?= $i ?>">
                                        <h5 class="mb-0">
                                          <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $i ?>" aria-expanded="true" aria-controls="collapse<?= $i ?>">
                                            <?= get_sub_field('syllabus_title'); ?>
                                          </button>
                                        </h5>
                                      </div>

                                      <div id="collapse<?= $i ?>" class="collapse" aria-labelledby="heading<?= $i ?>" data-parent="#accordion">
                                        <div class="card-body">
                                          <?= get_sub_field('syllabus_content'); ?>
                                        </div>
                                      </div>
                                    </div>
                            <?php
                            $i++;
                          endwhile;?>
                          </div>
                          <?php endif;
                           endwhile;
                           endif;
                          ?>
                          </div>

                          <div class="tab-pane fade" id="schedule-pricing" role="tabpanel" aria-labelledby="schedule-pricing-tab">
                           <div class="summary-content">
                             <?= get_field('schedule_pricing'); ?>
                           </div>
                         </div>

                          </div>


          </div>
					<div class="col-md-3">
									<div class="single_post_img">
									<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
								</div>

					</div>
					<?php
			 endwhile; // End of the loop.
			 ?>
        </div>
      </div>
      </div>
    </section>
<?php get_footer(); ?>
