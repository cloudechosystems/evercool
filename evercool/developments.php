<?php
/*
Template Name: Developments page Template
*/
?>
<?php get_header(); ?>
<?php
 $loop = new WP_Query( array( 'post_type' => 'development', 'posts_per_page' => -1, 'order' => 'ASC','orderby' => 'menu_order') );
 if ( $loop->have_posts() ) :
     while ( $loop->have_posts() ) : $loop->the_post(); ?>
     <div class="news_item foo">
	<div class="row">
		<div class="col-md-3">
			<div class="news_thumbnail_img">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
		<div class="col-md-9">
			<span class="news_date"><?php echo get_the_date(); ?></span>
			<div class="news_title_wrapper">
				<a href="<?php echo get_the_permalink(); ?>" class="news_title">
					<?php echo get_the_title(); ?>
				</a>
			</div>
			<div class="news_content">
				<?php echo wp_trim_words(get_the_content(),40); ?>
			</div>
			<div class="news_links">
				<a href="<?php echo get_the_permalink(); ?>">Continue reading</a>
			</div>
		</div>

	</div>
</div>
     <?php endwhile;

 endif;
 wp_reset_postdata();
?>
<?php get_footer(); ?>
