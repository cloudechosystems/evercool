
          <div class="container">
           <div class="row">
            <div class="col-md-9">
                      <?php
                      while ( have_posts() ) :
                          the_post();?>
                          <div class="single_post">
                            <div class="single_post_img">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                          </div>
                          <div class="single_post_date">
                            <h3><?php the_date(); ?></h3>
                          </div>
                          <div class="single_post_title">
                            <h1><?php the_title() ;?></h1>
                          </div>
                          <div class="single_post_content">

                              <?php the_content(); ?>

                          </div>
                           </div>
                           <div class="single_post_navigation">
                           <?php
                            the_post_navigation();?>
                        </div>
                        <?php
                          // If comments are open or we have at least one comment, load up the comment template.
                          if ( comments_open() || get_comments_number() ) :
                            ?>
                             <div class="single_post_form">
                               <?php
                              comments_template();
                              ?>
                            </div>
                            <?php
                          endif;

                      endwhile; // End of the loop.
                      ?>
          </div>
        </div>
      </div>
