<?php
/**
 * evercool functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package evercool
 */

if ( ! function_exists( 'evercool_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function evercool_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on evercool, use a find and replace
		 * to change 'evercool' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'evercool', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'evercool' ),
			'footer' => esc_html__( 'footer', 'evercool' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'evercool_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'evercool_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function evercool_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'evercool_content_width', 640 );
}
add_action( 'after_setup_theme', 'evercool_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function evercool_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'evercool' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'evercool' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'evercool_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function evercool_scripts() {
	wp_enqueue_style( 'evercool-style', get_stylesheet_uri() );

	wp_enqueue_script( 'evercool-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'evercool-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'evercool_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function starter_customize_register( $wp_customize )
{
    $wp_customize->add_section( 'footer_section' , array(
        'title'    => __( 'Footer section', 'starter' ),
        'priority' => 60
    ) );

		$wp_customize->add_setting( 'footer_logo' , array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo', array(
		'label'    => __( 'Footer logo', 'starter' ),
		'section'  => 'footer_section',
		'settings' => 'footer_logo',
		) ) );



		$wp_customize->add_setting( 'footer_facebook' , array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_facebook', array(
        'label'    => __( 'Facebook link', 'starter' ),
        'section'  => 'footer_section',
        'settings' => 'footer_facebook',
    ) ) );


		$wp_customize->add_setting( 'footer_twitter' , array(
				'default'   => '',
				'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_twitter', array(
				'label'    => __( 'Twitter link', 'starter' ),
				'section'  => 'footer_section',
				'settings' => 'footer_twitter',
		) ) );


		$wp_customize->add_setting( 'footer_linkedin' , array(
		'default'   => '',
		'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_linkedin', array(
				'label'    => __( 'Linkedin link', 'starter' ),
				'section'  => 'footer_section',
				'settings' => 'footer_linkedin',
		) ) );


		$wp_customize->add_setting( 'footer_address' , array(
		'default'   => '',
		'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_address', array(
				'label'    => __( 'Address', 'starter' ),
				'section'  => 'footer_section',
				'settings' => 'footer_address',
		) ) );


		$wp_customize->add_setting( 'footer_address_line2' , array(
		'default'   => '',
		'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_address_line2', array(
				'label'    => __( 'Address Line 2', 'starter' ),
				'section'  => 'footer_section',
				'settings' => 'footer_address_line2',
		) ) );


		$wp_customize->add_setting( 'footer_phone' , array(
		'default'   => '',
		'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_phone', array(
				'label'    => __( 'Phone', 'starter' ),
				'section'  => 'footer_section',
				'settings' => 'footer_phone',
		) ) );


		$wp_customize->add_setting( 'footer_email' , array(
		'default'   => '',
		'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_email', array(
				'label'    => __( 'Email', 'starter' ),
				'section'  => 'footer_section',
				'settings' => 'footer_email',
		) ) );


		$wp_customize->add_setting( 'footer_image_1' , array(
				'default'   => '',
				'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_image_1', array(
		'label'    => __( 'Footer Image 1', 'starter' ),
		'section'  => 'footer_section',
		'settings' => 'footer_image_1',
		) ) );

		$wp_customize->add_setting( 'footer_image_2' , array(
				'default'   => '',
				'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_image_2', array(
		'label'    => __( 'Footer Image 2', 'starter' ),
		'section'  => 'footer_section',
		'settings' => 'footer_image_2',
		) ) );


		$wp_customize->add_setting( 'footer_image_3' , array(
				'default'   => '',
				'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_image_3', array(
		'label'    => __( 'Footer Image 3', 'starter' ),
		'section'  => 'footer_section',
		'settings' => 'footer_image_3',
		) ) );


		$wp_customize->add_setting( 'footer_certification_content' , array(
		'default'   => '',
		'transport' => 'refresh',
		) );
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_certification_content', array(
				'label'    => __( 'Certification Content', 'starter' ),
				'section'  => 'footer_section',
				'settings' => 'footer_certification_content',
		) ) );


}
add_action( 'customize_register', 'starter_customize_register');
