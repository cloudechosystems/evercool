<?php
/*
Template Name: Training page Template
*/
?>
<?php get_header(); ?>
<section class="inner_page_banner" style="background-image: url('<?php echo get_field('banner_image') ?>')">
	<div class="container">
		<div class="page_title">
	    <?php echo get_field('banner_heading') ?>
		</div>
		<div class="page_description">
	    <?php echo get_field('banner_sub_heading') ?>
		</div>
	</div>
</section>

<section class="section">
  <div class="container-fluid">
  	<div class="row">
		<?php
		 $loop = new WP_Query( array( 'post_type' => 'training', 'posts_per_page' => -1, 'order' => 'ASC','orderby' => 'menu_order') );
		 if ( $loop->have_posts() ) :
		     while ( $loop->have_posts() ) : $loop->the_post(); ?>
		      <div class="col-md-3">
		      <div class="training_item">
		        <a href="<?php echo get_the_permalink(); ?>" class="">
		          <?php the_post_thumbnail(); ?>
							<div class="training_name">
								<?php echo get_the_title(); ?>
							</div>

		        </a>
		        </div>
					  </div>
		     <?php endwhile;

		 endif;
		 wp_reset_postdata();
		?>
</div>
</div>
</section>
<?php get_footer(); ?>
