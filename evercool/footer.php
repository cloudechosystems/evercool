<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package evercool
 */

?>

	</div><!-- #content -->

<a href="#" id="contact" class="anchor"></a>
	<footer id="colophon" class="site-footer">
		<div class="footer_wrapper"><!--footer-content-area -->
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="footer_content"><!--footer-copywrite-info -->
							<div class="footer_logo_wrapper"><!--copywrite_text  -->
									<div class="footer_logo">
										<a class="" href="">
											  <img src="<?= get_theme_mod( 'footer_logo' ); ?>" alt="" />
                      </a>
									</div>
									<p><?= get_theme_mod( 'footer_address' ); ?></p>
									<p><?= get_theme_mod( 'footer_address_line2' ); ?></p>
									<p><?= get_theme_mod( 'footer_phone' ); ?></p>
									<p><?= get_theme_mod( 'footer_email' ); ?></p>
							</div><!--copywrite_text close -->
							<div class="footer_social"><!--footer-social-info  -->
								<?php if(get_theme_mod( 'footer_facebook' )): ?>
								<a class="" href="<?= get_theme_mod( 'footer_facebook' ); ?>">
												<i class="fab fa-facebook-f" aria-hidden="true"></i>
                </a>
								<?php endif; ?>

								<?php if(get_theme_mod( 'footer_twitter' )): ?>
								<a class="" href="<?= get_theme_mod( 'footer_twitter' ); ?>">
												<i class="fab fa-twitter" aria-hidden="true"></i>
								</a>
								<?php endif; ?>

								<?php if(get_theme_mod( 'footer_linkedin' )): ?>
								<a class="" href="<?= get_theme_mod( 'footer_linkedin' ); ?>">
												<i class="fab fa-linkedin-in" aria-hidden="true"></i>
								</a>
								<?php endif; ?>
							</div><!--footer-social-info end  -->
						</div><!--footer-copywrite-info end-->
					</div>

					<div class="col-md-3">
						<div id="" class="footer_contact_wrapper"><!--contact_info_area-->
							<div class="footer_contact_box"><!--contact_info -->
								<h5>ENQUIRE</h5>
							<?php echo do_shortcode( '[contact-form-7 id="139" title="Footer"]' ); ?>
							</div><!--contact_info end-->
						</div><!--contact_info_area end-->
					</div>
					<div class="col-md-3">
						<div class="map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28870.019008234423!2d55.33322743370504!3d25.245264017910145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf5e623f1618d6827!2sEvercool%20air-conditioning!5e0!3m2!1sen!2sin!4v1591092750709!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

						</div>

					</div>
					<div class="col-md-3">

					</div>

				</div>
			</div>
			<div class="site-info">
				<a href="<?php echo esc_url( __( 'https://www.cloudechosystems.com/', 'evercool' ) ); ?>">
					<?php
					/* translators: %s: CMS name, i.e. WordPress. */
					printf( esc_html__( 'Proudly powered by %s', 'evercool' ), 'CloudEcho Systems' );
					?>
				</a>

			</div><!-- .site-info -->

		</div>

	</footer><!-- #colophon -->
	<button onclick="topFunction()" id="scrolltopBtn" title="Go to top"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>

	<div class="dialer_button">

		<a href="tel: +97142363856 " class="dialer_button_call"> <span class="cta_phone_number">+97142363856</span>  <i class="fas fa-phone"></i></a>

	<a href="mailto:info@evercooluae.com" class="dialer_button_email"><i class="far fa-envelope"></i></a>
		<a href="https://wa.me/0585646439" class="dialer_button_whatsapp" target="_blank">
			<i class="fab fa-whatsapp my-float"></i>
		</a>
	</div>
</div><!-- #page -->
<!-- pop up model -->
<div class="">
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
					<div class="contact_box">
						<h5 class="model_title" style="text-align: center;">Apply</h5>
 	 				  <?php echo do_shortcode( '[contact-form-7 id="311" title="careers job form"]' ); ?>
 	 			</div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- pop up model end -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<?php wp_footer(); ?>


<script type="text/javascript">
jQuery('.nav-menu li:first-child').addClass('without-after-element');

</script>
<script src="<?php echo get_template_directory_uri(); ?>/asset/vendor/owl.carousel.min.js"></script>

<script>
	$(document).ready(function() {
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			loop: true,
			margin: 10,
			autoplay: true,
			autoplayTimeout: 4000,
			autoplayHoverPause: true,
			responsive:true,
  		responsive:{
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1024: {
      items: 4
    }
  }
		});
	})
</script>
<script>
//Get the button
var mybutton = document.getElementById("scrolltopBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>
<script type="text/javascript">
// vars
"use strict";
var testim = document.getElementById("testim"),
	testimDots = Array.prototype.slice.call(
		document.getElementById("testim-dots").children
	),
	testimContent = Array.prototype.slice.call(
		document.getElementById("testim-content").children
	),
	testimLeftArrow = document.getElementById("left-arrow"),
	testimRightArrow = document.getElementById("right-arrow"),
	testimSpeed = 4500,
	currentSlide = 0,
	currentActive = 0,
	testimTimer,
	touchStartPos,
	touchEndPos,
	touchPosDiff,
	ignoreTouch = 30;
window.onload = function () {
	// Testim Script
	function playSlide(slide) {
		for (var k = 0; k < testimDots.length; k++) {
			testimContent[k].classList.remove("active");
			testimContent[k].classList.remove("inactive");
			testimDots[k].classList.remove("active");
		}

		if (slide < 0) {
			slide = currentSlide = testimContent.length - 1;
		}

		if (slide > testimContent.length - 1) {
			slide = currentSlide = 0;
		}

		if (currentActive != currentSlide) {
			testimContent[currentActive].classList.add("inactive");
		}
		testimContent[slide].classList.add("active");
		testimDots[slide].classList.add("active");

		currentActive = currentSlide;

		clearTimeout(testimTimer);
		testimTimer = setTimeout(function () {
			playSlide((currentSlide += 1));
		}, testimSpeed);
	}

	testimLeftArrow.addEventListener("click", function () {
		playSlide((currentSlide -= 1));
	});

	testimRightArrow.addEventListener("click", function () {
		playSlide((currentSlide += 1));
	});

	for (var l = 0; l < testimDots.length; l++) {
		testimDots[l].addEventListener("click", function () {
			playSlide((currentSlide = testimDots.indexOf(this)));
		});
	}

	playSlide(currentSlide);

	// keyboard shortcuts
	document.addEventListener("keyup", function (e) {
		switch (e.keyCode) {
			case 37:
				testimLeftArrow.click();
				break;

			case 39:
				testimRightArrow.click();
				break;

			case 39:
				testimRightArrow.click();
				break;

			default:
				break;
		}
	});

	testim.addEventListener("touchstart", function (e) {
		touchStartPos = e.changedTouches[0].clientX;
	});

	testim.addEventListener("touchend", function (e) {
		touchEndPos = e.changedTouches[0].clientX;

		touchPosDiff = touchStartPos - touchEndPos;

		console.log(touchPosDiff);
		console.log(touchStartPos);
		console.log(touchEndPos);

		if (touchPosDiff > 0 + ignoreTouch) {
			testimLeftArrow.click();
		} else if (touchPosDiff < 0 - ignoreTouch) {
			testimRightArrow.click();
		} else {
			return;
		}
	});
};

</script>
<script>
	$(document).ready(function() {
		$("li.menu-item-has-children").click(function(){
			if ($( ".menu-item-has-children" ).hasClass( "toggled" )){
				$(".menu-item-has-children").removeClass("toggled");
			}
			else {
				$(".menu-item-has-children").addClass("toggled");

			}

		});
	});
</script>


</body>
</html>
