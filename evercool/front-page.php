<?php get_header(); ?>

<!-- ======================banner start===================== -->

<section class="Homepage_banner align-items-center" >
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">

        <?php
        if( have_rows('slider') ):
          $i=0;
            while( have_rows('slider') ) : the_row();   ?>
             <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i  ?>" class="<?=($i==0) ? "active":""; ?>"></li>


             <?php $i++;  endwhile;

           else :

           endif;?>

      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <?php

        if( have_rows('slider') ):
          $i=0;
          while( have_rows('slider') ) : the_row();   ?>

          <div class="carousel-item <?=($i==0) ? "active":""; ?>" style="background-image: url('')">
            <img class="d-block w-100" src="<?= get_sub_field('banner_image'); ?>" alt="">

            <div class="carousel-caption ">
              <h2 class="main_heading"> <?= get_sub_field('banner_heading'); ?></h2>
              <p class="main_heading_content"> <?= get_sub_field('banner_sub_heading'); ?></p>
              <a class="btn main_btn btn_spacing" href=" <?= get_sub_field('banner_button_link'); ?>"><?= get_sub_field('banner_button_text'); ?></a>
            </div>
          </div>
          <?php $i++;  endwhile;

          else :

          endif;?>

        </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
          </a>
    </div>

</section>

<!-- ======================banner ends===================== -->

<!-- ========================about start======================= -->

<a id="about" class="anchor" href="#"></a>
<section  class="section about">
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <div class="about_icon">
          <i class="fas fa-quote-left"></i>

        </div>
      </div>
      <div class="col-md-8">
        <div class="about_content">
          <p><?= get_field('about_content'); ?>  </p>
        </div>

      </div>
      <div class="col-md-2">

      </div>
    </div>

  </div>
</section>
<!-- ========================about ends======================= -->
<!-- ==========================core_services start===================== -->
<a id="service" class="anchor" href="#"></a>
<section  class="core_services section">
  <div class="container">
    <div class="">    <!-- section heading -->

      <h2 class="section-heading ">Our Core Services</h2>
    </div><!-- section heading end-->
    <div class="row row-eq-height">
      <?php
        if( have_rows('services') ):
            while ( have_rows('services') ) : the_row(); ?>
      <div class="col-12 col-sm-6 col-lg-3">
        <a href="<?php echo get_sub_field('link') ?>">
        <div class="core_services_wrapper">
          <div class="core_services_item_icon">
            <img class="" src="<?php echo get_sub_field('image') ?>" alt="">
          </div>
          <h6>  <?php echo get_sub_field('title') ?>  </h6>
          <p><?php  echo get_sub_field('content');  ?>  </p>
        </div>
        </a>
      </div>
      <?php  endwhile;
        else :
        endif;
        ?>
    </div>

  </div>
</section>

<!-- ==========================core_services ends===================== -->

<!-- ======================maintainance_section start======================= -->

<section class="section maintainance_section">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="row provide_box align-items-center">
          <span>
            <i class="fas fa-file-contract"></i>

          </span>
          <h5>Annual Maintenance <br> Contracts</h5>
        </div>

      </div>
      <div class="col-md-4">
        <div class="row provide_box align-items-center">
          <span>
            <i class="fas fa-headset"></i>

          </span>
          <h5>On Call Service <br>Division</h5>
        </div>

      </div>
      <div class="col-md-4">
        <div class="row provide_box align-items-center">
          <span>
            <i class="fas fa-cogs"></i>


          </span>


          <h5>Installations</h5>
        </div>

      </div>

    </div>

  </div>


</section>

<!-- ======================maintainance_section ends======================= -->

<!-- =====================experience start======================= -->

<section class="section experience " >
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="experience_heading">4 DECADES OF EXPERIENCE</h2>
        <h5 class="experience_sub_heading">in Air Conditioning Services</h5>

       <div class=" row experience_box_wrapper ">
         <div class="experience_box col-md-6">
           <span>
            <i class="far fa-clock"></i>
           </span>
          <h5>24/7 availibility</h5>

         </div>
         <div class="experience_box col-md-6">
           <span>
            <i class="fas fa-hard-hat"></i>
           </span>
          <h5>Experienced workers</h5>

         </div>
         <div class="experience_box col-md-6">
           <span>
             <i class="fas fa-mobile-alt"></i>
           </span>
          <h5>Free AMC check-up</h5>

         </div>
         <div class="experience_box col-md-6">
           <span>
             <i class="fas fa-money-bill-wave"></i>
           </span>
          <h5>Free estimate</h5>

         </div>


       </div>

           </div>
           <div class="col-md-6">
             <div class="experience_cnt">
               <h5>REQUEST A FREE QUOTE</h5>
             <?php echo do_shortcode( '[contact-form-7 id="197" title="experience"]' ); ?>

               </div>
             </div>
           </div>


      </div>

    </div>

  </div>
</section>

<!-- =====================experience ends======================= -->

<!-- ====================dealers start====================== -->

<a id="dealers" class="anchor" href="#"></a>
<section id="" class="section dealers">
  <div class="container">
    <div class="">
      <h2 class="section-heading">Major Dealing Brands</h2>
      <div class=" align-items-center owl-carousel owl-theme ">
        <?php
          if( have_rows('major_dealing_brands') ):
              while ( have_rows('major_dealing_brands') ) : the_row(); ?>
        <div class="text-center item">
          <img src="<?php echo get_sub_field('band_logo') ?>" alt="">
        </div>
        <?php  endwhile;
          else :
          endif;
          ?>
      </div>

    </div>

  </div>
</section>

<!-- ====================dealers ends====================== -->

<!-- ===============we specialise start================== -->

<section class="we_specialise_in section">
  <div class="container">
    <div >

      <h2 class="section-heading ">We Specialise In</h2>
    </div><!-- section heading end-->
    <div class="row ">
      <?php
        if( have_rows('we_specialise') ):
          while ( have_rows('we_specialise') ) : the_row(); ?>

          <div class="col-12 col-md-4">
            <div class="we_specialise_in_item text-center" >

              <div class="we_specialise_in_item_img">
                <img src="<?= get_sub_field('we_specialise_image'); ?>" alt="">


              </div>

              <div class="we_specialise_content">
                <h6><?= get_sub_field('we_specialise_heading'); ?></h6>
                <p>  <?= get_sub_field('we_specialise_content'); ?></p>
              </div>

            </div>

          </div>
        <?php endwhile;
        else :
          // no rows found
        endif;
        ?>


    </div>
  </div>
</section>

<!-- ===============we specialise ends================== -->

<!-- ===================testimonial start===================== -->

<section class="section testimonial_section">

  <div id="testim" class="testim">
    <!--         <div class="testim-cover"> -->
    <div class="wrap">

      <span id="right-arrow" class="arrow right fa fa-chevron-right"></span>
      <span id="left-arrow" class="arrow left fa fa-chevron-left "></span>

      <div id="testim-content" class="cont">
        <?php

        if( have_rows('testimonial') ):
          $i=0;
          while( have_rows('testimonial') ) : the_row();   ?>

          <div class=" <?=($i==0) ? "active":""; ?>">
            <div class="img"><img src="<?= get_sub_field('testimonial_image'); ?>" alt=""></div>
            <h2><?= get_sub_field('testimonial_name'); ?></h2>
            <p><?= get_sub_field('testimonial_content'); ?></p>
          </div>
          <?php $i++;  endwhile;

          else :

          endif;?>



      </div>

      <ul id="testim-dots" class="dots">
        <li class="dot active"></li>
        <!--
                    -->
        <li class="dot"></li>
        <!--
                    -->
        <li class="dot"></li>
        <!--
                    -->
        <li class="dot"></li>
        <!--
                    -->
        <li class="dot"></li>
      </ul>

    </div>
    <!--         </div> -->
  </div>

</section>
<!-- ===================testimonial ends===================== -->

<!-- ===================clients start=================== -->
<a id="clients" class="anchor" href="#"></a>

<section id="" class="section clients">
  <div class="container">
    <div >
      <h2 class="section-heading">Our Major Clients</h2>

    </div>
    <div class="row align-items-center">

      <?php
        if( have_rows('our_major_clients') ):
            while ( have_rows('our_major_clients') ) : the_row(); ?>
      <div class="col-6 col-md-2 text-center">
        <img src="<?php echo get_sub_field('client_logo') ?>" alt="">
      </div>
      <?php  endwhile;
        else :
        endif;
        ?>
    </div>
  </div>
</section>

<!-- ===================clients ends=================== -->







<?php get_footer(); ?>
