<?php get_header(); ?>
<section class="inner_page_banner" style="background-image: url('<?php echo get_field('banner_image') ?>')">
	<div class="container">

			<h1 class="page_title">
		    <?php echo get_field('banner_heading') ?>
			</h1>

		<div class="page_description">
	    <?php echo get_field('banner_sub_heading') ?>
		</div>
	</div>
</section>
<section class="section">
          <div class="container">
						<div class="row">
							<div class="col-md-12">
								<h2 class="section-heading"><?= get_field('service_heading'); ?></h2>
								<p><?= get_field('service_content'); ?></p>
							</div>

						</div>

          </div>


    </section>
    <?php get_footer(); ?>
