<?php
/*
Template Name: Contact Page Template
*/
?>
<?php get_header(); ?>
<section class="inner_page_banner">
    <div class="container-fluid">
      <div class="row">
        <?php
             if( have_rows('branch') ):?>
             <?php
                     while ( have_rows('branch') ) : the_row(); ?>
            <?= get_sub_field('map'); ?>
          <?php
        endwhile;
        endif;
            ?>

          </div>
    </div>
</section>
<section class="contact_area section">
  <div class="container">
    <div class="row contact_info">
      <?php
           if( have_rows('branch') ):?>
           <?php
                   while ( have_rows('branch') ) : the_row(); ?>
    <div class="col-md-6">
       <img class="" src="<?php echo get_template_directory_uri(); ?>/asset/img/contact-page-vector.png" alt="">

    </div>             
      <div class="col-md-6 address_card">
        <div class="row">
          <div class="col-md-4 address_card_left">
            <div class="address_card_logo">
              <img class="" src="<?php echo get_template_directory_uri(); ?>/asset/img/LOGO-mockup-02--white.png" alt="">


            </div>


          </div>
          <div class="col-md-8 address_card_right ">
            <div class="" id="map1">
              <!-- address_wrapper address_cards -->
              <h6>Contact Address</h6>
              <div class="branch_name">
                    <p class="contact_address"><?= get_sub_field('name'); ?></p>
              </div>
              <div class="branch_location">
                    <p class="contact_address"><?= get_sub_field('location'); ?></p>
              </div>
              <div class="branch_address">
                    <p class="contact_address"><?= get_sub_field('address'); ?></p>
              </div>
              <div class="branch_email">
                <img class="" src="<?php echo get_template_directory_uri(); ?>/asset/img/email.png" alt="">
                <p class="address_heading">Email</p>
                <a class="address_content" href="mailto:<?= get_sub_field('email'); ?>"><?= get_sub_field('email'); ?></a>
              </div>
              <div class="branch_phone">
                  <img class="" src="<?php echo get_template_directory_uri(); ?>/asset/img/telephone.png" alt="">
                <p class="address_heading">Phone</p>
                  <a class="address_content" href="callto:<?= get_sub_field('phone'); ?>"><?= get_sub_field('phone'); ?></a>
              </div>
            </div>
          </div>

        </div>

      </div>

      <!-- <div class="col-md-4">
        <div class="email_wrapper address_cards">
          <h6>Contact Details</h6>
          <div class="branch_email">
            <img class="" src="<?php echo get_template_directory_uri(); ?>/asset/img/email.png" alt="">
            <p class="address_heading">Email</p>
            <a class="address_content" href="mailto:<?= get_sub_field('email'); ?>"><?= get_sub_field('email'); ?></a>
          </div>
          <div class="branch_phone">
              <img class="" src="<?php echo get_template_directory_uri(); ?>/asset/img/telephone.png" alt="">
            <p class="address_heading">Phone</p>
              <a class="address_content" href="callto:<?= get_sub_field('phone'); ?>"><?= get_sub_field('phone'); ?></a>
          </div>
        </div>
    </div> -->


    <?php endwhile;
                  else :
                      // no rows found
                  endif;
                  ?>

    </div>
  </div>

</section>
<section class="contact_section section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-heading text-center">
            <div class="feature_img justify-content-center ">
                <img class="" src="<?php echo get_template_directory_uri(); ?>/asset/img/contact_icon.svg" alt="">
            </div>
            <h2>Contact With Us</h2>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-12 col-md-10 col-lg-8">
          <div class="contact_box">
            <?php echo do_shortcode( '[contact-form-7 id="72" title="contact section form"]' ); ?>
          </div>

        </div>

      </div>
    </div>

</section>
<?php get_footer();?>
