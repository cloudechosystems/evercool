<?php
/*
Template Name: About Page Template
*/
?>
<?php get_header(); ?>
<section class="inner_page_banner" style="background-image: url('<?php echo get_field('banner_image') ?>')">
	<div class="container">
		<h1 class="page_title">
	    <?php echo get_field('banner_heading') ?>
		</h1>
		<div class="page_description">
	    <?php echo get_field('banner_sub_heading') ?>
		</div>
	</div>
</section>
    <section class="section about_us">
      <div class="container">
          <div class="row align-items-center">
            <div class="col-md-12 ">
							<h5 class="section-heading"><?= get_field('about_us_heading'); ?></h5>
							<p><?= get_field('about_us_content'); ?></p>
            </div>

          </div>
        </div>
    </section>
    <section class="section about_us_history">
      <div class="container">
          <div class="row align-items-center">
            <div class="col-md-12">
							<h5 class="section-heading"><?= get_field('history_heading'); ?></h5>
							<p><?= get_field('history_content'); ?></p>

              </div>
            </div>

          </div>
    </div>
    </section>
    <section class="section ">
      <div class="container">
        <div class="row ">
          <div class="col-md-6 ">
						<div class="our_vision">
							<h5 class="section-heading"><?= get_field('our_vision_heading'); ?> </h5>
	            <p><?= get_field('our_vision_content'); ?></p>
						</div>

          </div>
          <div class="col-md-6">
            <div class="our_mission">
							<h5 class="section-heading"><?= get_field('our_mission_heading'); ?></h5>
	            <p><?= get_field('our_mission_content'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- <section class="section team">
      <div class="container">
        <h5 class="about_section_title">TEAM</h5>
        <div class="row ">
                        <?php
                  if( have_rows('team') ):
                    // loop through the rows of data
                      while ( have_rows('team') ) : the_row(); ?>
                  <div   class="col">
                    <div class="team_wrapper">
                      <div class="team_img">
                        <img class="" src="<?= get_sub_field('image'); ?>" alt="">
                      </div>
                      <div class="team_item_content">
                        <div class="team_name">
                          <b>
                            <?= get_sub_field('name'); ?></b>
                        </div>
                        <div class="team_designation">
                          <?= get_sub_field('designation'); ?>
                        </div>
                        <a class="team_link" href="mailto:<?= get_sub_field('email'); ?>"><?= get_sub_field('email'); ?></a>
                        <a class="team_link" href="callto:<?= get_sub_field('phone'); ?>"><?= get_sub_field('phone'); ?></a>
                      </div>
                    </div>
                  </div>
                  <?php endwhile;
              else :
                  // no rows found
              endif;
              ?>
        </div>
      </div>
    </section> -->
<?php get_footer();?>
