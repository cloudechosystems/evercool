<?php
/*
Template Name: Careers Page Template
*/
?>
<?php get_header(); ?>
<section class="inner_page_banner" style="background-image: url('<?php echo get_field('banner_image') ?>')">
	<div class="container">
		<h1 class="page_title">
	    <?php echo get_field('banner_heading') ?>
		</h1>
		<div class="page_description">
	    <?php echo get_field('banner_sub_heading') ?>
		</div>
	</div>
</section>
<section class="careers">
	<div class="container">
		<div class="row">
			<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					/*
					 * Include the Post-Type-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
					 */
					 get_template_part( 'template-parts/content', 'career' );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
	</div>
</div>
</section>
<?php get_footer();?>
