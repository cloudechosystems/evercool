<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package evercool
 */

get_header();
?>
<section class="inner_page_banner single_training" >
	<div class="container">
		<h1 class="page_title">
	    <?php echo get_field('banner_heading') ?>
		</h1>
		<div class="page_description">
	    <?php echo get_field('banner_sub_heading') ?>
		</div>
	</div>
</section>
<section  class="section">
	<div class="container">
		<div class="row">
			<div class="col-12">
		<?php
		while ( have_posts() ) :
			the_post();?>
			<div class="single_job_title single_job col-md-12">
			<?php
			get_template_part( 'template-parts/content', get_post_type() );
			?>
			</div>
			<?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
	</div>
</div>
</div>
</section>

<?php
get_footer();
